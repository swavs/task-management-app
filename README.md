This repository consists of both backend and frontend code. The root folder for the project has backend code using nestjs framework and mongodb. 
## Local backend setup

- Have environment file `.env` with below values

```DB_URL=mongodb://localhost:27017/tasksdb```

```JWT_SECRET=DO NOT USE THIS VALUE. INSTEAD, CREATE A COMPLEX SECRET AND KEEP IT SAFE OUTSIDE OF THE SOURCE CODE.```

- Have `test.env` file with below values for the test to run
```DB_URL_TEST=mongodb://localhost:27017/testdb```

The url for the local dev and test db can be changed to the values according to the local db setup. This setup also uses docker instance of mongodb, to use the docker instance run `docker-compose up` to start mongodb.

- Install dependencies using `npm install` from root folder.
- Start backend server using `npm run backend:dev` from root folder.
- Backend url will be `http://localhost:3001/api/v1/`
- To run unit tests run `npm run test`
- To run e2e tests, run `npm run test:e2e`
- To add initial user to db, use POST request to url `http://localhost:3001/api/v1/users` with payload:

```js
   {
    "name": "test",
    "email": "test@bar1.com",
    "password": "Test12345$"
   }
```

## Local backend setup
- Go to the `/frontend` folder e.g `cd frontend`
- Have environment file `.env` with below values, at `/frontend` folder

```REACT_APP_API_URL=http://localhost:3001/```

- install dependencies using `npm install`
- start local frontend server using `npm start`
- Frontend url will be `http://localhost:3000/`
- If user is not logged in, it will be redirected login page, enter the user credentials created before to login.

## Todos
- Frontend tasks list uses Material UI table which does not have much customization options for the server side pagination and hence this needs more investigation.
- Unit tests for frontend.
- Backend tests fail sometimes randomly due to timeout errors, jest configuration for the same needs optimisation fix.
- More testing and UI error handling and display for the tasks CRUD features.
