
import { INestApplication, ValidationPipe, HttpStatus } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as request from 'supertest';
import { JwtService } from '@nestjs/jwt';
import { AuthGuard } from '../src/auth/guards/auth.guard';
import { AuthModule } from '../src/auth/auth.module';
import { TasksModule } from '../src/tasks/tasks.module';
import { TasksService } from '../src/tasks/tasks.service';
import { TaskDto } from '../src/tasks/task.dto';
import { Task } from '../src/tasks/task.entity';
import { Status } from '../src/tasks/status.enum';
import {
  createTestConfiguration,
  createTestTypeOrmOptions,
  mockJwtService,
  mockJwtPayload,
} from './utils';

describe('TasksController (e2e)', () => {
  let app: INestApplication;
  let service: TasksService;
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(createTestConfiguration()),
        TypeOrmModule.forRoot(createTestTypeOrmOptions([Task])),
        TypeOrmModule.forFeature([Task]),
        AuthModule,
        TasksModule,
      ],
      providers: [
        {
          provide: APP_GUARD,
          useClass: AuthGuard,
        },
        TasksService,        
      ],
    })
    .overrideProvider(JwtService)
    .useValue(mockJwtService)
    .compile();

    app = module.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();

    service = module.get<TasksService>(TasksService);
  });

  afterAll(async () => {
    await service.deleteAll();
    await module.close();
    await app.close();
  });

  it('/tasks (POST) - Unauthorized request without jwt', () => {
    const task: TaskDto = {
      title: 'Test',
      description: 'A test task',
      status: Status.Todo,
    };
    return request(app.getHttpServer())
      .post('/tasks')
      .set('Accept', 'application/json')
      .send(task)
      .expect(HttpStatus.UNAUTHORIZED);
  });

  it('/tasks (POST) - Bad request without task data', async () => {
    const task: TaskDto = {
      title: '',
      description: 'A test task',
      status: Status.Todo,
    };
    return request(app.getHttpServer())
      .post('/tasks')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer test-token')
      .send(task)
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('/tasks (GET) - return tasks', async () => {
    const task: TaskDto = {
      title: 'Test task',
      description: 'A test task',
      status: Status.Todo,
    };
    await service.create(task, mockJwtPayload);
    return request(app.getHttpServer())
      .get('/tasks?page=1')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer test-token')
      .expect(HttpStatus.OK);
  });
});
