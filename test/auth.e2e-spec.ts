
import { INestApplication, ValidationPipe, HttpStatus } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as request from 'supertest';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { AuthModule } from '../src/auth/auth.module';
import { AuthService } from '../src/auth/auth.service';
import { User } from '../src/users/user.entity';
import { UsersService } from '../src/users/users.service';
import {
  createTestConfiguration,
  createTestTypeOrmOptions,
  mockJwtService,
} from './utils';

describe('AuthController (e2e)', () => {
  let app: INestApplication;
  let userService: UsersService;
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(createTestConfiguration()),
        TypeOrmModule.forRoot(createTestTypeOrmOptions([User])),
        TypeOrmModule.forFeature([User]),
        AuthModule,
      ],
      providers: [
        AuthService,
        UsersService,
      ],
    })
    .overrideProvider(JwtService)
    .useValue(mockJwtService)
    .compile();

    app = module.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();

    userService = module.get<UsersService>(UsersService);
    jest.spyOn(bcrypt, 'compare').mockImplementation(async (password: string, _: string) => password === 'Test12345$');
  });

  afterAll(async () => {
    await userService.deleteAll();
    await module.close();
    await app.close();
  });

  it('/auth/login (POST) - Bad request without user data', () => {
    return request(app.getHttpServer())
      .post('/auth/login')
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('/auth/login (POST) - Unauthorized with invalid user data', async () => {
    
    await userService.create({ name: 'Tester', email: 'tester@foo.com', password: 'Test12345$' });
    const user = {
      email: 'tester@foo.com',
      password: 'inavlid-password',
    }
    return request(app.getHttpServer())
      .post('/auth/login')
      .set('Accept', 'application/json')
      .send(user)
      .expect(HttpStatus.UNAUTHORIZED);
  });

  it('/auth/login (POST) - successful login with valid user data', async () => {
    
    await userService.create({ name: 'Tester', email: 'tester@bar.com', password: 'Test12345$' });
    const user = {
      email: 'tester@bar.com',
      password: 'Test12345$',
    }
    return request(app.getHttpServer())
      .post('/auth/login')
      .set('Accept', 'application/json')
      .send(user)
      .expect(HttpStatus.OK);
  });
});
