import { ConfigModuleOptions } from '@nestjs/config';
import { JwtSignOptions, JwtVerifyOptions } from '@nestjs/jwt';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { EntitySchema } from 'typeorm';
import { JWTPayload } from '../src/auth/auth.service';
import { Role } from '../src/auth/role.enum';

type Entity = Function | string | EntitySchema<any>;

export const createTestConfiguration = (): ConfigModuleOptions => ({
    envFilePath: 'test.env',
});

export const createTestTypeOrmOptions = (
  entities: Entity[],
): TypeOrmModuleOptions => ({
    type: 'mongodb',
    url: process.env.DB_URL_TEST,
    entities,
    autoLoadEntities: true,
    synchronize: true,
});

export const mockJwtPayload: JWTPayload = {
  id: '65fc250e43fa89fe96857da8',
  roles: [ Role.User ],
  name: 'test',
};

export const mockJwtService = {
  signAsync: jest.fn().mockImplementation(async (payload: object, _: JwtSignOptions) => {
    return {
      accessToken: payload.toString(),
    };
  }),
  verifyAsync: jest.fn().mockImplementation(async (token: string, _: JwtVerifyOptions) => {
    return mockJwtPayload;
  }),
};
