
import { INestApplication, ValidationPipe, HttpStatus } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as request from 'supertest';
import { User } from '../src/users/user.entity';
import { UsersModule } from '../src/users/users.module';
import { UsersService } from '../src/users/users.service';
import { createTestConfiguration, createTestTypeOrmOptions } from './utils';

describe('UsersController (e2e)', () => {
  let app: INestApplication;
  let service: UsersService;
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(createTestConfiguration()),
        TypeOrmModule.forRoot(createTestTypeOrmOptions([User])),
        TypeOrmModule.forFeature([User]),
        UsersModule,
      ],
    }).compile();

    app = module.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();

    service = module.get<UsersService>(UsersService);
  });

  afterAll(async () => {
    await service.deleteAll();
    await module.close();
    await app.close();
  });

  it('/users (POST) - bad request without user data', () => {
    return request(app.getHttpServer())
      .post('/users')
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('/users (POST) - bad request with invalid data', () => {
    const user = {
      name: '',
      email: 'invalid-email',
      password: 'inavlid-password',
    }
    return request(app.getHttpServer())
      .post('/users')
      .set('Accept', 'application/json')
      .send(user)
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('/users (POST) - create new user with invalid data', () => {
    const user = {
      name: 'test',
      email: 'test@foo.com',
      password: 'Test12345$',
    }
    return request(app.getHttpServer())
      .post('/users')
      .set('Accept', 'application/json')
      .send(user)
      .expect(HttpStatus.CREATED);
  });
});
