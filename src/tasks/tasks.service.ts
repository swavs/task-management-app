import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ObjectId } from 'mongodb';
import { MongoRepository } from 'typeorm';
import { Document, DeleteResult } from 'typeorm/driver/mongodb/typings';
import { Task } from './task.entity';
import { TaskDto } from './task.dto';
import { JWTPayload } from '../auth/auth.service';
import { PAGE_SIZE } from './constants';

type PaginationResult = {
  data: Task[],
  pageInfo: {
    hasNext: boolean,
    hasPrev: boolean,
    page: number,
  }
};

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(Task)
    private readonly tasksRepository: MongoRepository<Task>,
  ) {}

  private async validateUser(taskId: string, user: JWTPayload): Promise<void> {
    const task = await this.findOneById(taskId);
    if (task?.userId.toString() !== user.id) {
      throw new UnauthorizedException();
    }
  }
  async findOneById(taskId: string): Promise<Task | undefined> {
    const user = await this.tasksRepository.findOneBy({ _id: new ObjectId(taskId) });    
    return user;
  }

  async findAll(page: number, status: string, user: JWTPayload): Promise<PaginationResult> {
    const findQuery = {
      where: { userId: new ObjectId(user.id), ...(status ? { status } : {}) },
      skip: (page - 1) * PAGE_SIZE,
      take: PAGE_SIZE + 1,
    }
    const results =  await this.tasksRepository.find(findQuery);
    return {
      data: results.slice(0, PAGE_SIZE),
      pageInfo: {
        hasNext: results.length > PAGE_SIZE,
        hasPrev: page > 1,
        page,
      }
    };
  }

  async create(task: TaskDto, user: JWTPayload): Promise<Task> {
    return this.tasksRepository.save({ ...task, userId: new ObjectId(user.id) });
  }

  async update(taskId: string, update: TaskDto, user: JWTPayload): Promise<Document> {
    await this.validateUser(taskId, user);
    return this.tasksRepository.findOneAndUpdate({ _id: new ObjectId(taskId) }, { $set: update });
  }

  async delete(taskId: string, user: JWTPayload): Promise<DeleteResult> {
    await this.validateUser(taskId, user);
    return this.tasksRepository.deleteOne({ _id: new ObjectId(taskId) });
  }

  async deleteAll(): Promise<DeleteResult> {
    return this.tasksRepository.deleteMany({});
  }
}
