import { IsNotEmpty, IsEnum } from 'class-validator';
import { Status } from './status.enum';

export class TaskDto {
  @IsNotEmpty()
  title: string;

  description: string;

  @IsNotEmpty()
  @IsEnum(Status)
  status: Status;
}
