import { Column, Entity, ObjectId, ObjectIdColumn, Index } from 'typeorm';
import { ObjectId as ObjectID } from 'mongodb';
import { Status } from './status.enum';

@Entity()
export class Task {
  @ObjectIdColumn()
  _id: ObjectId;

  @Column({
    nullable: false,
  })
  @Index({ fulltext: true })
  title: string;

  @Column()
  description: string;

  @Column({
    nullable: false,
  })
  status: Status;

  @Column({
    nullable: false,
  })
  userId: ObjectID;
}
