import { ConfigModule } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TasksService } from './tasks.service';
import { Task } from './task.entity';
import { TaskDto } from './task.dto';
import { Status } from './status.enum';
import {
  createTestConfiguration,
  createTestTypeOrmOptions,
  mockJwtPayload,
  mockJwtService,
} from '../../test/utils';

describe('TasksService', () => {
  let service: TasksService;
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(createTestConfiguration()),
        TypeOrmModule.forRoot(createTestTypeOrmOptions([Task])),
        TypeOrmModule.forFeature([Task]),
      ],
      providers: [
        TasksService,
        { provide: JwtService, useValue: mockJwtService }     
      ],
    }).compile();

    service = module.get<TasksService>(TasksService);
    await service.deleteAll();
  });

  afterAll(async () => {
    await service.deleteAll();
    await module.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create, update and delete task', async () => {
    // Create task
    const task: TaskDto = {
      title: 'Test task',
      description: 'A test task',
      status: Status.Todo,
    };
    const created = await service.create(task, mockJwtPayload);
    expect(created.title).toBe('Test task');

    // update task
    const toUpdate: TaskDto = {
      title: 'Test title',
      description: 'A test task updated',
      status: Status.InProgress,
    };
    await service.update(created._id.toString(), toUpdate, mockJwtPayload);
    const updated = await service.findOneById(created._id.toString());
    expect(updated.title).toBe('Test title');
    expect(updated.description).toBe('A test task updated');
    expect(updated.status).toBe('In Progress');

    // Delete task
    await service.delete(created._id.toString(), mockJwtPayload);
    const deleted = await service.findOneById(created._id.toString());
    expect(deleted).toBeFalsy();
  });

  it('should find all the tasks with pagination', async () => {
    // Create tasks
    const task1: TaskDto = {
      title: 'Test task 1',
      description: 'A test task',
      status: Status.Todo,
    };
    const task2: TaskDto = {
      title: 'Test task 2',
      description: 'A test task',
      status: Status.Todo,
    };
    const task3: TaskDto = {
      title: 'Test task 3',
      description: 'A test task',
      status: Status.InProgress,
    };
    await service.create(task1, mockJwtPayload);
    await service.create(task2, mockJwtPayload);
    await service.create(task3, mockJwtPayload);
    
    const result = await service.findAll(1, undefined, mockJwtPayload);
    expect(result.data.length).toBe(3);
    expect(result.pageInfo.hasNext).toBe(false);
    expect(result.pageInfo.hasPrev).toBe(false);

    const statusResult = await service.findAll(1, Status.InProgress, mockJwtPayload);
    expect(statusResult.data.length).toBe(1);
  });

  it('should throw error when taskId is not created by user', async () => {
    try {
      // passing some invalid/ non existing task id
      await await service.delete('65fc250e43fa89fe96857da8', mockJwtPayload);
    } catch(error: unknown) {
      expect(error?.toString().includes('UnauthorizedException')).toBeTruthy();
    }
  });
});
