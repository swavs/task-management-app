import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Req,
  Param,
  Put,
  Query,
} from '@nestjs/common';
import { Roles } from '../auth/decorators/roles.decorator';
import { Role } from '../auth/role.enum';
import { TasksService } from './tasks.service';
import { TaskDto } from './task.dto';

export type Query = {
  page: number,
  status?: string,
};

@Controller('tasks')
export class TasksController {
  constructor(
    private readonly tasksService: TasksService,
  ) {}  

  @Roles(Role.User)
  @HttpCode(HttpStatus.CREATED)
  @Post()
  async create(@Body() taskDto: TaskDto, @Req() request: Request) {
    return this.tasksService.create(taskDto, request['user']);
  }

  @Roles(Role.User)
  @HttpCode(HttpStatus.OK)
  @Put('/:taskId')
  async update(@Param('taskId') taskId: string, @Body() taskDto: TaskDto, @Req() request: Request) {
    return this.tasksService.update(taskId, taskDto, request['user']);
  }

  @Roles(Role.User)
  @HttpCode(HttpStatus.OK)
  @Delete('/:taskId')
  async delete(@Param('taskId') taskId: string, @Req() request: Request) {
    return this.tasksService.delete(taskId, request['user']);
  }

  @Roles(Role.User)
  @HttpCode(HttpStatus.OK)
  @Get()
  async find(@Query() query: Query, @Req() request: Request) {
    return this.tasksService.findAll(query.page, query.status, request['user']);
  }
}
