import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { User } from './user.entity';
import { createTestConfiguration, createTestTypeOrmOptions } from '../../test/utils';

describe('UsersService', () => {
  let service: UsersService;
  let module: TestingModule;
  const testUser = { name: 'Tester', email: 'tester@bar.com', password: 'Test12345$' };

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(createTestConfiguration()),
        TypeOrmModule.forRoot(createTestTypeOrmOptions([User])),
        TypeOrmModule.forFeature([User]),
      ],
      providers: [
        UsersService,        
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
    await service.deleteAll();
  });

  afterAll(async () => {
    await service.deleteAll();
    await module.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create user and find user by email', async () => {
    await service.create(testUser);
    const user = await service.findOneBy({ email: 'tester@bar.com' });
    expect(user.email).toBe('tester@bar.com');
  });

  it('should create unique user', async () => {
    try {
      // Create same testUser again
      await service.create(testUser);
    } catch(error: unknown) {
      expect(error?.toString().includes('BadRequestException: User already exists.')).toBeTruthy();
    }
  });
});
