import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { DeleteResult } from 'typeorm/driver/mongodb/typings';
import * as bcrypt from 'bcrypt';
import { User } from './user.entity';
import { UserDto } from './user.dto';
import { Role } from '../auth/role.enum';
import { SALT_ROUNDS } from './user.contants';

type CreateResult = {
  created: boolean,
};

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: MongoRepository<User>,
  ) {}

  async findOneBy(options: Record<string, any>): Promise<User | undefined> {
    const user = await this.usersRepository.findOneBy(options);    
    return user;
  }

  async create(user: UserDto): Promise<CreateResult> {
    const existingUser = await this.findOneBy({ email: user.email });    
    if (existingUser) {
      throw new BadRequestException('User already exists.');
    }
    const password = await bcrypt.hash(user.password, SALT_ROUNDS);
    await this.usersRepository.save({ ...user, password, roles: [Role.User] });
    return { created: true };
  }

  async deleteAll(): Promise<DeleteResult> {
    return this.usersRepository.deleteMany({});
  }
}
