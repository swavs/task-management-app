import { Column, Entity, ObjectId, ObjectIdColumn, Index } from 'typeorm';
import { IsEmail } from 'class-validator';
import { Role } from '../auth/role.enum';

@Entity()
export class User {
  @ObjectIdColumn()
  _id: ObjectId;

  @Column({
    nullable: false,
  })
  @Index({ unique: true })
  @IsEmail()
  email: string;

  @Column({
    nullable: false,
  })
  name: string;

  @Column({
    nullable: false,
  })
  password: string;

  @Column({
    nullable: false,
  })
  roles: Role[];
}
