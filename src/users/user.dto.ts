import { IsEmail, IsNotEmpty, IsStrongPassword } from 'class-validator';

export class UserDto {
  @IsEmail()
  email: string;

  @IsNotEmpty()
  name: string;

  @IsStrongPassword(
    {
      minLength: 8,
      minNumbers: 1,
      minSymbols: 1,
      minUppercase: 1,
    },
    {
      message: 'Password must be minimum of 8 characters, with minimun of 1 upper case, 1 number and 1 special character.',
    },
  )
  password: string;
}
