import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Req,
} from '@nestjs/common';
import { Public } from '../auth/decorators/public.decorator';
import { Roles } from '../auth/decorators/roles.decorator';
import { Role } from '../auth/role.enum';
import { UsersService } from './users.service';
import { UserDto } from './user.dto';

@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
  ) {}  

  @Public()
  @HttpCode(HttpStatus.CREATED)
  @Post()
  async register(@Body() userDto: UserDto) {
    return this.usersService.create(userDto);
  }
  
  @Roles(Role.User)
  @HttpCode(HttpStatus.OK)
  @Get('/current')
  async getUser(@Req() request: Request) {
    // return current logged in user.
    return request['user'];
  }
}
