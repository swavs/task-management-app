import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { AuthService } from './auth.service';
import { UsersModule } from '../users/users.module';
import { UsersService } from '../users/users.service';
import { User } from '../users/user.entity';
import { createTestConfiguration, createTestTypeOrmOptions, mockJwtService } from '../../test/utils';

describe('AuthService', () => {
  let authService: AuthService;
  let userService: UsersService;
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(createTestConfiguration()),
        TypeOrmModule.forRoot(createTestTypeOrmOptions([User])),
        TypeOrmModule.forFeature([User]),
        UsersModule,
      ],
      providers: [
        AuthService,
        UsersService,
        { provide: JwtService, useValue: mockJwtService },
      ],
    })
    .compile();

    authService = module.get<AuthService>(AuthService);
    userService = module.get<UsersService>(UsersService);
    await userService.deleteAll();
    
    jest.spyOn(bcrypt, 'compare').mockImplementation(async (password: string, _: string) => password === 'Test12345$');
  });

  afterAll(async () => {
    await userService.deleteAll();
    await module.close();
  });

  it('should be defined', () => {
    expect(authService).toBeDefined();
  });

  it('should throw exception when user does not exist', async () => {
    try {
      // User does not exist in database
      await authService.login({ email: 'tester@foobar.com', password: 'Test12345$' });
    } catch(error: unknown) {
      expect(error?.toString().includes('UnauthorizedException')).toBeTruthy();
    }
  });

  it('should return jwt when login is successful', async () => {
    await userService.create({ name: 'Tester', email: 'tester@foo.com', password: 'Test12345$' });
    const loginResult = await authService.login({ email: 'tester@foo.com', password: 'Test12345$' });
    expect(loginResult.accessToken).toBeTruthy();
  });
});
