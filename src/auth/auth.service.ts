import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { UsersService } from '../users/users.service';
import { UserDto } from './user.dto';
import { jwtConstants } from './constants';
import { Role } from './role.enum';

type UserSignedIn = {
  accessToken: string;
};

export type JWTPayload = {
  name: string;
  id: string;
  roles: Role[],
};

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async login(userDto: UserDto): Promise<UserSignedIn> {
    const user = await this.usersService.findOneBy({ email: userDto.email });    
    if (!user || !(await bcrypt.compare(userDto.password, user.password))) {
      throw new UnauthorizedException();
    }

    const payload: JWTPayload = {
      name: user.name,
      id: user._id.toString(),
      roles: user.roles,
    };
    return {
      accessToken: await this.jwtService.signAsync(payload, {
        secret: process.env.JWT_SECRET,
        expiresIn: jwtConstants.expiresIn
      }),
    };
  }
}
