Frontend using React and JS, uses Material UI for the styled reusable react components.
## Local setup:
Have .env file with below variables:

```REACT_APP_API_URL=http://localhost:3001/```

To install dependencies, run `npm install` from `/frontend`

To start frontend server, run `npm start`