import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Loading from '../Components/Loading';

export const UserContext = React.createContext();

const UserContextProvider = ({ children }) => {
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState(undefined);

  useEffect(() => {
    if (!user && window.localStorage.getItem('token')) {
      setLoading(true);
      axios.defaults.headers = { Authorization: `Bearer ${window.localStorage.getItem('token')}` };
      axios
        .get('/users/current')
        .then(result => {
          setLoading(false);
          setUser(result.data);
        });
    } else {
      setLoading(false);
    }
  }, [user]);

  if (loading) return <Loading />;

  return (
    <UserContext.Provider
      value={{ user }}
    >
      {children}
    </UserContext.Provider>
  );
};

export default UserContextProvider;
