import React from 'react';
import { useNavigate } from 'react-router-dom';

const Logout = () => {
  const navigate = useNavigate();

  React.useEffect(() => {
    window.localStorage.setItem('token', '');
    navigate('/login');
  }, [navigate]);

  return null;
};

export default Logout;
