import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Loading from '../Components/Loading';
import ErrorMessage from '../Components/ErrorMessage';
import { apiBaseURl } from '../config';

const SignIn = () => {
  useEffect(() => {
    window.localStorage.setItem('token', '');
  }, []);

  const navigate = useNavigate();
  const [emailError, setEmailError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [apiError, setApiError] = useState(false);
  const [invalidLogin, setInvalidLogin] = useState(false);
  const [loading, setLoading] = useState(false);

  const resetErrors = () => {
    setEmailError(false);
    setPasswordError(false);
    setApiError(false);
    setInvalidLogin(false);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    resetErrors();
    const data = new FormData(event.currentTarget);
    const formData = {
      email: data.get('email'),
      password: data.get('password'),
    };
    if (!formData.email) {
      setEmailError(true);
      return;
    }
    if (!formData.password) {
      setPasswordError(true);
      return;
    }

    setLoading(true);
    try {
      const {
        data: { accessToken },
      } = await axios('/auth/login', {
        baseURL: apiBaseURl,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        data: JSON.stringify(formData),
      });

      window.localStorage.setItem('token', accessToken);
      axios.defaults.headers = { Authorization: `Bearer ${accessToken}` };
      navigate('/');
    } catch (e) {
      if (e.response?.status === 401) {
        setInvalidLogin(true);
      } else {
        setApiError(true);
      }
      console.error(e);
    } finally {
      setLoading(false);
    }
  };

  if (loading) return <Loading />;

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        {apiError && (
          <ErrorMessage message="Sorry...There was an error and login failed." />
        )}
        {invalidLogin && (
          <ErrorMessage message="Invalid email or password." />
        )}
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <Box
          component="form"
          onSubmit={handleSubmit}
          noValidate
          sx={{ mt: 1 }}
        >
          <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            error={emailError}
            helperText={emailError ? "Enter valid email." : undefined}
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            error={passwordError}
            helperText={passwordError ? "Enter password." : undefined}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign In
          </Button>
        </Box>
      </Box>
    </Container>
  );
};

export default SignIn;
