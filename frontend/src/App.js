import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import Layout from './Layout/Content';
import UserContextProvider from './Context/UserContext';
import { apiBaseURl } from './config';

axios.defaults.baseURL = apiBaseURl;

const App = () => {
  const navigate = useNavigate();

  useEffect(() => {
    if (!window.localStorage.getItem('token')) {
      navigate('/login');
    }
  });

  return (
    <UserContextProvider>
      <Layout />
    </UserContextProvider>
  );
};

export default App;
