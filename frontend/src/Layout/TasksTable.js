import React, { useEffect, useState, useCallback } from 'react';
import Box from '@mui/material/Box';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Checkbox from '@mui/material/Checkbox';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import AddIcon from '@mui/icons-material/Add';
import axios from 'axios';
import Loading from '../Components/Loading';
import TaskDialog from './TaskDialog';

const headCells = [
    {
        id: 'title',
        label: 'Task title',
    },
    {
        id: 'status',
        label: 'Status',
    },
];

const defaultTask = {
    title: '',
    description: '',
    status: 'To Do',
};

const TasksTableHead = () => {
    return (
        <TableHead>
            <TableRow>
                <TableCell padding="checkbox">
                </TableCell>
                {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align={'left'}
                        padding={'normal'}
                    >
                        {headCell.label}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}


const TasksTableToolbar = (props) => {
    const { numSelected, onStatusChange, selectedStatus, onDeleteClick, onAddClick, onEditClick } = props;

    const handleStatusChange = (event) => {
        onStatusChange(event.target.value);
    };

    return (
        <Toolbar
            sx={{
                pl: { sm: 2 },
                pr: { xs: 1, sm: 1 },
                display: 'flex',
                justifyContent: 'space-between',
            }}
        >
            {numSelected > 0 ? (
                <Typography
                    color="inherit"
                    variant="subtitle1"
                    component="div"
                >
                    Manage selected task
                </Typography>
            ) : (
                <Typography
                    variant="h6"
                    id="tableTitle"
                    component="div"
                >
                    Tasks
                </Typography>
            )}
            {numSelected > 0 ? (
                <Box sx={{
                    display: 'flex',
                    justifyContent: 'space-between',
                }}>
                    <Tooltip title="Delete">
                        <IconButton onClick={onDeleteClick}>
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="Edit">
                        <IconButton onClick={onEditClick}>
                            <EditIcon />
                        </IconButton>
                    </Tooltip>
                </Box>
            ) : (
                <Box sx={{
                    display: 'flex',
                    justifyContent: 'space-between',
                }}>
                    <Tooltip title="Create a task">

                        <IconButton onClick={onAddClick}>
                            <AddIcon />
                        </IconButton>

                    </Tooltip>
                    <Box sx={{
                        display: 'flex',
                        alignItems: 'center'
                    }}>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            label="Status"
                            onChange={handleStatusChange}
                            value={selectedStatus}
                            sx={{ ml: 2, mr: 2, minWidth: 120, height: 40 }}
                        >
                            <MenuItem value={'All'}>All</MenuItem>
                            <MenuItem value={'To Do'}>To Do</MenuItem>
                            <MenuItem value={'In Progress'}>In Progress</MenuItem>
                            <MenuItem value={'Done'}>Done</MenuItem>
                        </Select>
                    </Box>
                </Box>
            )}
        </Toolbar>
    );
}

const TasksTable = () => {
    const [selected, setSelected] = useState(-1);
    const [page, setPage] = useState(0);
    const [rows, setRows] = useState({ data: [] });
    const [loading, setLoading] = useState(false);
    const [reload, setReload] = useState(false);
    const [status, setStatus] = useState('All');
    const [isDialogOpen, setIsDialogOpen] = useState(false);
    const [task, setTask] = useState(defaultTask);

    const loadTasks = useCallback(() => {
        setLoading(true);
        let url = `/tasks?page=${page + 1}`;
        if (status && status !== 'All') {
            url = `${url}&status=${status}`;
        }
        axios.get(url).then(result => {
            setRows(result.data);
            setLoading(false);
            setReload(false);
        })
            .catch(error => {
                console.log(error);
                setLoading(false);
            });
    }, [page, status]);

    useEffect(() => {
        if (reload || !rows.pageInfo) {
            loadTasks();
        }
    }, [page, reload, status, rows.pageInfo, loadTasks]);

    const handleRowClick = (taskIndex) => {
        setSelected(taskIndex);
    };

    const handleChangePage = async (_, newPage) => {
        let reload = false;
        if (page < newPage && rows.pageInfo?.hasNext) {
            reload = true;
        } else if (page > newPage && rows.pageInfo?.hasPrev) {
            reload = true;
        }
        if (reload) {
            setPage(newPage);
            setReload(true);
        }
    };

    const handleSelectedTaskChange = (event, taskIndex) => {
        if (event.target.checked) {
            setSelected(taskIndex);
        } else {
            setSelected(-1);
        }
    };

    const onStatusChange = async (status) => {
        setPage(0);
        setStatus(status);
        setReload(true);
    };

    const onDeleteClick = async () => {
        const taskId = rows.data[selected]?._id;
        await axios.delete(`/tasks/${taskId}`);
        setPage(0);
        setSelected(-1);
        setStatus('All');
        setReload(true);
    };

    const onAddClick = () => {
        setTask(defaultTask);
        setIsDialogOpen(true);
    };

    const onEditClick = () => {
        const selectedTask = rows.data[selected];
        setTask(selectedTask);
        setIsDialogOpen(true);
    };

    const onCloseDialog = (reload) => {
        setTask(defaultTask);
        setIsDialogOpen(false);
        if (reload) {
            setSelected(-1);
            setReload(true);
        }
    };

    const isSelected = (index) => selected === index;

    if (loading) return <Loading />;

    return (
        <>
            <Box sx={{ width: '100%' }}>
                <Paper sx={{ width: '100%', mb: 2 }}>
                    <TasksTableToolbar
                        numSelected={selected >= 0 ? 1 : -1}
                        onStatusChange={onStatusChange}
                        selectedStatus={status}
                        onDeleteClick={onDeleteClick}
                        onEditClick={onEditClick}
                        onAddClick={onAddClick}
                    />
                    <TableContainer>
                        <Table
                            sx={{ minWidth: 750 }}
                            aria-labelledby="tableTitle"
                            size={'medium'}
                        >
                            <TasksTableHead />
                            <TableBody>
                                {rows.data.map((row, index) => {
                                    const isItemSelected = isSelected(index);
                                    const labelId = `enhanced-table-checkbox-${index}`;

                                    return (
                                        <TableRow
                                            hover
                                            onClick={() => handleRowClick(index)}
                                            role="checkbox"
                                            aria-checked={isItemSelected}
                                            tabIndex={-1}
                                            key={row._id}
                                            selected={isItemSelected}
                                            sx={{ cursor: 'pointer' }}
                                        >
                                            <TableCell padding="checkbox">
                                                <Checkbox
                                                    color="primary"
                                                    checked={isItemSelected}
                                                    inputProps={{
                                                        'aria-labelledby': labelId,
                                                    }}
                                                    onChange={(event) => handleSelectedTaskChange(event, index)}
                                                />
                                            </TableCell>
                                            <TableCell
                                                component="th"
                                                id={labelId}
                                                scope="row"
                                                padding="normal"
                                            >
                                                {row.title}
                                            </TableCell>
                                            <TableCell>{row.status}</TableCell>
                                        </TableRow>
                                    );
                                })}
                                {rows.data.length === 0 && (
                                    <TableRow
                                        style={{
                                            height: (53) * 1,
                                        }}
                                    >
                                        <TableCell colSpan={6} align='center' >
                                            There are no tasks, create a new task.
                                        </TableCell>
                                    </TableRow>
                                )}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        component="div"
                        count={-1}
                        rowsPerPage={20}
                        page={page}
                        onPageChange={handleChangePage}
                        rowsPerPageOptions={[20, 50, 100]}
                    />
                </Paper>
            </Box>
            <TaskDialog task={task} onCloseDialog={onCloseDialog} isOpen={isDialogOpen} />
        </>
    );
};

export default TasksTable;
