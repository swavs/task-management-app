import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import axios from 'axios';
import Loading from '../Components/Loading';

const TaskDialog = ({ isOpen, task, onCloseDialog }) => {

    const [loading, setLoading] = React.useState(false);
    const handleCancel = () => {
        onCloseDialog(false);
    };

    const onSubmit = async (event) => {
        event.preventDefault();
        setLoading(true);
        const formData = new FormData(event.currentTarget);
        const taskData = Object.fromEntries(formData.entries());
        const mode = task._id ? 'edit' : 'create';
        if (mode === 'create') {
            await axios.post(`/tasks`, taskData);
        } else {
            await axios.put(`/tasks/${task._id}`, taskData);
        }
        setLoading(false);
        onCloseDialog(true);
    };

    if (loading) return <Loading />;

    return (
        <React.Fragment>
            <Dialog
                open={isOpen}
                onClose={handleCancel}
                PaperProps={{
                    component: 'form',
                    onSubmit,
                }}
            >
                <DialogTitle>{task._id ? 'Edit Task' : 'Create Task'}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Add or update task details.
                    </DialogContentText>
                    <TextField
                        autoFocus
                        required
                        margin="dense"
                        id="title"
                        name="title"
                        label="Task title"
                        type="text"
                        fullWidth
                        variant="standard"
                        defaultValue={task.title}
                    />
                    <TextField
                        margin="dense"
                        id="description"
                        name="description"
                        label="Task description"
                        type="text"
                        fullWidth
                        variant="standard"
                        defaultValue={task.description}
                    />
                    <Select

                        required
                        labelId="demo-simple-select-label"
                        id="status"
                        name="status"
                        label="Status"
                        variant="standard"
                        fullWidth
                        defaultValue={task.status}
                        sx={{ mt: 2 }}
                    >
                        <MenuItem value={'To Do'}>To Do</MenuItem>
                        <MenuItem value={'In Progress'}>In Progress</MenuItem>
                        <MenuItem value={'Done'}>Done</MenuItem>
                    </Select>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCancel}>Cancel</Button>
                    <Button type="submit">Save</Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
};

export default TaskDialog;
