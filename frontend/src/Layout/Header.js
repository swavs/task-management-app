import React from 'react';
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import { deepOrange } from '@mui/material/colors';

const stringAvatar = (name) => {
  const parts = name.split(' ')
  return {
    sx: {
      bgcolor: deepOrange[500],
    },
    children: `${parts[0][0]}${parts?.length > 1 ? parts[1][0] : ''}`,
  };
}

const Header = ({ user }) => (
  <div style={{ width: '100%' }}>
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row-reverse',
        p: 1,
        m: 1.5,
        bgcolor: 'background.paper',
      }}
    >
      <Avatar {...stringAvatar(user.name)} />
    </Box>
  </div>

);

export default Header;
