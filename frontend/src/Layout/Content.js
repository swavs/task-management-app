import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';
import Header from './Header';
import Loading from '../Components/Loading';
import { UserContext } from '../Context/UserContext';
import EnhancedTable from './TasksTable';

const drawerWidth = 240;

const Page = ({ user }) => {

  const [mobileOpen, setMobileOpen] = useState(false);

  const navigate = useNavigate();

  if (!user) return <Loading />

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div>
      <Typography component="h2" variant="h6" color="primary" gutterBottom sx={{ p: 2 }}>
        Welcome, {user.name}!
      </Typography>
      <List>
        <ListItem disablePadding>
          <ListItemButton
            selected={true}
            onClick={() => navigate('/')}
          >
            <ListItemText primary="Dashboard" />
          </ListItemButton>
        </ListItem>
        <ListItem disablePadding>
          <ListItemButton
            selected={true}
            onClick={() => navigate('/logout')}
          >
            <ListItemText primary="Logout" />
          </ListItemButton>
        </ListItem>
      </List>
    </div>
  );

  return (
    <>
      <Header user={user} />
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <Box
          component="nav"
          sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
          aria-label="nav menu"
        >
          {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
          <Drawer
            variant="temporary"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
            sx={{
              display: { xs: "block", sm: "none" },
              "& .MuiDrawer-paper": {
                boxSizing: "border-box",
                width: drawerWidth,
              },
            }}
          >
            {drawer}
          </Drawer>
          <Drawer
            variant="permanent"
            sx={{
              display: { xs: "none", sm: "block" },
              "& .MuiDrawer-paper": {
                boxSizing: "border-box",
                width: drawerWidth,
              },
            }}
            open
          >
            {drawer}
          </Drawer>
        </Box>
        <Box
          component="main"
          sx={{
            flexGrow: 1,
            width: { sm: `calc(100% - ${drawerWidth}px)` },
          }}
        >
          <EnhancedTable />
        </Box>
      </Box>
    </>
  );
};

const Content = () => (
  <UserContext.Consumer>
    {({ user }) => <Page user={user} />}
  </UserContext.Consumer>
);
export default Content;
