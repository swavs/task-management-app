import React from 'react';
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';

const Loading = () => (
  <Box sx={{ m: 8, textAlign: 'center' }}>
    <CircularProgress color="primary" />
  </Box>
);

export default Loading;
